const express = require('express');
const app = express();
const favicon = require('serve-favicon');
const path = require('path');
const router = require('express').Router();
const index = router.get('/', (req, res) => {
    res.render('html', { title: "WebMusic" });
});
const git = router.get('/git', (req, res) => {
    res.render('git');
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(index);
app.use(git);
app.use(favicon(path.join(__dirname, 'public', 'Favicon.ico')));
app.use(express.static('public'));
app.listen(2000);
module.exports = app;