const app = require('./app');
const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({ port: 8080 });
wss.setMaxListeners(1);
const server = require('net').createServer(socket => {
    socket.on('data', data => {
        wss.emit('ondata', JSON.parse(data.toString()))
    });
    socket.on('error', err => {
        return err.message
    });
}).listen(3000)
wss.on('connection', function(ws) {
    ws.on('error', error => {
        console.log(error.message);
    });
    ws.on('close', () => {
        ws.close();
    });
});
wss.on('ondata', data => {
    wss.clients.forEach(vaule => {
        console.log(vaule.upgradeReq.connection.address().address);
        if (vaule.upgradeReq.connection.address().address.indexOf(data.ip) > -1) {
            vaule.send(data.music)
            return;
        }
    })
});